import sys, os
sys.path.append(os.path.join(sys.path[0], 'jblock'))


config.load_autoconfig()
# config.editor.command = ['alacritty', '-e', 'nvim',  '-f', '{file}']
c.tabs.position = "bottom"
c.scrolling.bar = 'never'
c.statusbar.show = "in-mode"
c.scrolling.smooth = True
c.input.insert_mode.auto_load = True
c.zoom.default = '75%'
c.zoom.levels = ['25%', '33%', '50%', '67%', '75%', '90%', '100%', '110%', '125%', '150%', '175%', '200%', '250%', '300%', '400%', '500%']
config.source('qutebrowser-themes/themes/onedark.py')